package com.javarush.task.task14.task1420;

/* 
НОД
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

//        try {
            int a = Integer.parseInt(rd.readLine());
            int b = Integer.parseInt(rd.readLine());
            if ((a <= 0) || (b <= 0))
                throw new Exception();
//            if (a <= 0) throw new Exception();
//            if (b <= 0) throw new Exception();
            while (a != 0 && b != 0){
                if (a > b)
                    a = a % b;
                else
                    b = b % a;
            }

            int nod = a + b;
            System.out.println(nod);
//        } catch (NumberFormatException e) {
//            e.printStackTrace();
//        }
    }
}
