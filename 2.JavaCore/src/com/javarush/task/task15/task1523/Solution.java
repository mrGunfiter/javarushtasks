package com.javarush.task.task15.task1523;

/* 
Перегрузка конструкторов
*/

public class Solution {
    private int age;
    private String name;

    public static void main(String[] args) {

    }

    public Solution() {
    }

    private Solution(int a) {
        this.age = a;
    }

    protected Solution(String s) {
        this.name = s;
    }

    Solution(String s, int a) {
        this.age = a;
        this.name = s;
    }

}

