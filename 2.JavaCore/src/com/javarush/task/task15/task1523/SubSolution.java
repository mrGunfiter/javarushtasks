package com.javarush.task.task15.task1523;

public class SubSolution extends Solution {
    public SubSolution() {
        super();
    }

    protected SubSolution(String s) {
        super(s);
    }

    SubSolution(String s, int a) {
        super(s, a);
    }
}
