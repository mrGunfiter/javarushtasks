package com.javarush.task.task15.task1519;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.nio.Buffer;

/* 
Разные методы для разных типов
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        //напиште тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            String key = rd.readLine();
            if ("exit".equals(key))
                break;
            try {
                if (key.contains(".")) {
                    print(Double.parseDouble(key));
                    continue;
                }
                int iKey = Integer.parseInt(key);
                if ((iKey > 0) && (iKey < 128))
                    print((short) iKey);
                else if ((iKey <= 0) || (iKey >= 128))
                    print((Integer) iKey);
            } catch (NumberFormatException e) {
                print(key);
            }
        }
    }

    public static void print(Double value) {
        System.out.println("Это тип Double, значение " + value);
    }

    public static void print(String value) {
        System.out.println("Это тип String, значение " + value);
    }

    public static void print(short value) {
        System.out.println("Это тип short, значение " + value);
    }

    public static void print(Integer value) {
        System.out.println("Это тип Integer, значение " + value);
    }
}
