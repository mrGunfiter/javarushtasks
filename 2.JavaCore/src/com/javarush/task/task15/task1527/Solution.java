package com.javarush.task.task15.task1527;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.KeyPair;
import java.util.*;

/* 
Парсер реквестов

ttp://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo

ttp://javarush.ru/alpha/index.html?obj=3.14&name=Amigo
*/

public class Solution {

    public static void main(String[] args) throws IOException {
//        //add your code here
//        тоже работает правильно, но не принимает валидатор 28.06.2019
//        BufferedReader rd =  new BufferedReader(new InputStreamReader(System.in));
//        Map<String, String> map = new HashMap<>();
//        String key = null;
//        String value = null;
//        try {
//            String url = rd.readLine();
//            url = url.substring(url.indexOf("?") + 1);
//            String tmpStr = url.substring(0, url.indexOf("&"));
//            if (tmpStr.contains("=")) {
//                key = tmpStr.substring(0, tmpStr.indexOf("="));
//                value = tmpStr.substring(tmpStr.indexOf("=") + 1);
//            } else {
//                key = tmpStr.substring(0, tmpStr.indexOf("&"));
//                value = null;
//            }
//            map.put(key, value);
//            while ((url.contains("&")) || (url.contains("="))) {
//               url = url.substring(url.indexOf("&") + 1);
//               if (url.contains("&")) {
//                   tmpStr = url.substring(0, url.indexOf("&"));
//               } else if (url.contains("=")) {
//                   tmpStr = url;
//                   url = "";
//               }
//               if (tmpStr.contains("=")) {
//                   key = tmpStr.substring(0, tmpStr.indexOf("="));
//                   if (tmpStr.contains("&")) {
//                       value = tmpStr.substring(tmpStr.indexOf("="), tmpStr.indexOf("&"));
//                   } else {
//                       value = tmpStr.substring(tmpStr.indexOf("=") + 1);
//                   }
//               } else {
//                   key = tmpStr;
//                   value = null;
//               }
//               map.put(key, value);
//            }
//
//            String strKeys = "";
//            for (String mapKey : map.keySet()) {
//                strKeys += mapKey + " ";
//            }
//            System.out.println(strKeys.trim());
//            if (map.containsKey("obj")) {
//                String objValue = map.get("obj");
//                if (objValue.contains(".")) {
//                    try {
//                        double objVal = Double.parseDouble(objValue);
//                        alert(objVal);
//                    } catch (NumberFormatException e){
//                        alert(objValue);
//                    }
//                }
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

        String URL = new BufferedReader(new InputStreamReader(System.in)).readLine().replaceAll("^(.*?\\?)", "");
//        for (String s : URL.split("&")) System.out.print(s.replaceAll("=.*$", "") + " ");
        for(String s:URL.split("&")) {
            if (s.contains("=")) {
                System.out.print(s.substring(0, s.indexOf("=")) + " ");
            } else {
                System.out.print(s + " ");
            }
        }
        System.out.println();
        for (String s : URL.split("&")) {
            if (s.replaceAll("=.*$", "").equals("obj")) {
                try {
                    alert(Double.parseDouble(s = s.replaceAll("^[^=]*=", "")));
                } catch (NumberFormatException e) {
                    alert(s);
                }
            }
        }
    }

    public static void alert(double value) {
        System.out.println("double: " + value);
    }

    public static void alert(String value) {
        System.out.println("String: " + value);
    }
}
