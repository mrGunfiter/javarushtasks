package com.javarush.task.task15.task1514;

import java.util.HashMap;
import java.util.Map;

/* 
Статики-1
*/

public class Solution {
    public static Map<Double, String> labels = new HashMap<Double, String>();

    static {
        labels.put(5.0, "qqqq");
        labels.put(6.0, "qqqqq");
        labels.put(7.0, "qqqqqq");
        labels.put(8.0, "qqqqqqq");
        labels.put(9.0, "qqqqqqqq");
    }

    public static void main(String[] args) {
        System.out.println(labels);
    }
}
