package com.javarush.task.task17.task1710;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;

/*
CRUD
*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<Person>();
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    public static SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd-MMM-yyyy");

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) throws ParseException {
        //start here - начни тут
//        -c - добавляет человека с заданными параметрами в конец allPeople, выводит id (index) на экран
//        -u - обновляет данные человека с данным id
//                -d - производит логическое удаление человека с id, заменяет все его данные на null
//                -i - выводит на экран информацию о человеке с id: name sex (м/ж) bd (формат 15-Apr-1990)
        Date birthDate;
        switch (args[0]) {
            case ("-c"): {
                Person person = null;
                birthDate = dateFormat.parse(args[3]);
                if (args[2].equals("м")) {
                    person = Person.createMale(args[1], birthDate);
                    allPeople.add(person);
                    System.out.println(allPeople.size() - 1);
                } else if (args[2].equals("ж")) {
                    person = Person.createFemale(args[1], birthDate);
                    allPeople.add(person);
                    System.out.println(allPeople.size() - 1);
                }
                break;
            }
            case ("-u"): {
                birthDate = dateFormat.parse(args[4]);
                int num = Integer.parseInt(args[1]);
                if (args[3].equals("м")) {
                    allPeople.set(num, Person.createMale(args[2], birthDate));
                }
                else if (args[3].equals("ж")) {
                    allPeople.set(num, Person.createFemale(args[2],birthDate));
                }
                break;
            }
            case ("-d"): {
                allPeople.get(Integer.parseInt(args[1])).setSex(null);
                allPeople.get(Integer.parseInt(args[1])).setBirthDate(null);
                allPeople.get(Integer.parseInt(args[1])).setName(null);
                break;
            }
            case ("-i"): {
                Person viewPerson = allPeople.get(Integer.parseInt(args[1]));
                String sSex = "";
                if (viewPerson.getSex().equals(Sex.MALE))
                    sSex = "м";
                else if (viewPerson.getSex().equals(Sex.FEMALE))
                    sSex = "ж";


                System.out.println(viewPerson.getName() + " " + sSex + " " +  dateFormatOut.format(viewPerson.getBirthDate()));
                break;
            }
        }
    }
}
