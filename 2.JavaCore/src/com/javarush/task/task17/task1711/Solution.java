package com.javarush.task.task17.task1711;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD 2
*/

public class Solution {
    public static volatile List<Person> allPeople = new ArrayList<Person>();
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    public static SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
//    public static SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd-MMM-yyyy");

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) throws ParseException {
        //start here - начни тут
        Date birthDate;
        switch (args[0]) {
            case ("-c"): {
                synchronized (allPeople) {
                    for (int i = 1; i < args.length; i += 3) {
                        Person person = null;
                        birthDate = dateFormat.parse(args[i + 2]);
                        if (args[i + 1].equals("м")) {
                            person = Person.createMale(args[i], birthDate);
                            allPeople.add(person);
                            System.out.println(allPeople.size() - 1);
                        } else if (args[i + 1].equals("ж")) {
                            person = Person.createFemale(args[i], birthDate);
                            allPeople.add(person);
                            System.out.println(allPeople.size() - 1);
                        }
                    }
                }
                break;
            }
            case ("-u"): {
                synchronized (allPeople) {
                    for (int i = 1; i < args.length; i += 4) {
                        birthDate = dateFormat.parse(args[i + 3]);
                        int num = Integer.parseInt(args[i]);
                        if (args[i + 2].equals("м")) {
                            allPeople.set(num, Person.createMale(args[i + 1], birthDate));
                        } else if (args[i + 2].equals("ж")) {
                            allPeople.set(num, Person.createFemale(args[i + 1], birthDate));
                        }
                    }
                }
                break;
            }
            case ("-d"): {
                synchronized (allPeople) {
                    for (int i = 1; i < args.length; i++) {
                        allPeople.get(Integer.parseInt(args[i])).setSex(null);
                        allPeople.get(Integer.parseInt(args[i])).setBirthDate(null);
                        allPeople.get(Integer.parseInt(args[i])).setName(null);
                    }
                }
                break;
            }
            case ("-i"): {
                synchronized (allPeople) {
                    for (int i = 1; i < args.length; i++) {
                        Person viewPerson = allPeople.get(Integer.parseInt(args[i]));
                        String sSex = "";
                        if (viewPerson.getSex().equals(Sex.MALE))
                            sSex = "м";
                        else if (viewPerson.getSex().equals(Sex.FEMALE))
                            sSex = "ж";
                        System.out.println(viewPerson.getName() + " " + sSex + " " + dateFormatOut.format(viewPerson.getBirthDate()));
                    }
                }
                break;
            }
        }
    }
}
