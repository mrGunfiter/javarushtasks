package com.javarush.task.task13.task1318;

import java.io.*;
import java.util.Scanner;

/* 
Чтение файла
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        // напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        try {
            String fileName = rd.readLine();
            InputStream inStream = new FileInputStream(fileName);
            try {
                while (inStream.available() > 0) {
                    int data = inStream.read(); //читаем один байт из потока для чтения
                    System.out.print((char) data); //записываем прочитанный байт в другой поток.
                }
            } catch (Exception e) {
                System.out.println("Can't read file!");
            } finally {
                inStream.close();
            }

        } catch (IOException e) {
            System.out.println("Wrong file Name!");
        } finally {
            rd.close();
        }

    }
}