package com.javarush.task.task13.task1319;

import java.io.*;

/* 
Писатель в файл с консоли
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        // напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        String fileName = rd.readLine();

        FileWriter FileToWrite = new FileWriter(fileName); // поток который подключается к текстовому файлу
        BufferedWriter wr = new BufferedWriter(FileToWrite); // соединяем FileWriter с BufferedWitter

        while (true) {
            String str = rd.readLine();
            wr.write(str + System.lineSeparator());
            if (str.equals("exit"))
                break;
        }
        rd.close();
        wr.close();
    }
}
