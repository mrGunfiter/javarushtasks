package com.javarush.task.task13.task1326;

/* 
Сортировка четных чисел из файла
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class Solution {
    public static void main(String[] args) throws IOException {
        // напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<Integer> arr = new ArrayList<Integer>();
        try {
            String fileName = rd.readLine();
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
            String line;
            try {
                while ((line = br.readLine()) != null) {
                    Integer dig = Integer.parseInt(line);
                    if (dig % 2 == 0)
                        arr.add(dig);
                }
            } catch (NumberFormatException e) {
                System.out.println("String not digit");
            } finally {
                br.close();
            }
            Collections.sort(arr, new Comparator<Integer>() {
                    public int compare(Integer arr1, Integer arr2) {
                        return arr1.compareTo(arr2);
                    }
                }
            );
            for (Integer a: arr) {
                System.out.println(a);
            }
        } catch (IOException e) {
            System.out.println("Wrong file Name!");
        } finally {
            rd.close();
        }

    }
}
