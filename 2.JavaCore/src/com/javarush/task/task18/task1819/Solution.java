package com.javarush.task.task18.task1819;

/* 
Объединение файлов
*/

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            String fileName1 = reader.readLine();
            String fileName2 = reader.readLine();

            FileInputStream fis1 = new FileInputStream(fileName1);
            int i = 0;
            List<Integer> list = new ArrayList<Integer>();
            while ((i = fis1.read()) != -1) {
                list.add(i);
            }
            fis1.close();
            FileInputStream fis2 = new FileInputStream(fileName2);
            FileOutputStream fos = new FileOutputStream(fileName1);
            while ((i = fis2.read()) != -1) {
                fos.write(i);
            }
            fis2.close();
            for (Integer oneByte: list) {
                fos.write(oneByte);
            }
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
