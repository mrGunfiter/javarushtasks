package com.javarush.task.task18.task1803;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* 
Самые частые байты
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fileIn = new FileInputStream(rd.readLine());
        List<Integer> list = new ArrayList<>();
        Map<Integer, Integer> map = new HashMap<>();

        while (fileIn.available() > 0) {
            int oneByte = fileIn.read();
            map.put(oneByte, map.containsKey(oneByte)?(map.get(oneByte) + 1) : 1);
        }
        fileIn.close();
        int max = Integer.MIN_VALUE;
        for (Map.Entry<Integer, Integer> pair : map.entrySet()) {
            if (max < pair.getValue()) {
                max = pair.getValue();
            }
        }
        for (Map.Entry<Integer, Integer> pair : map.entrySet()) {
            if (max == pair.getValue()) {
                System.out.print(pair.getKey() + " ");
            }
        }
//        System.out.println(map);
    }
}
