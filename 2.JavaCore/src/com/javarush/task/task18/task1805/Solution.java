package com.javarush.task.task18.task1805;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* 
Сортировка байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fileIn = new FileInputStream(rd.readLine());
        List<Integer> list = new ArrayList<>();

        while (fileIn.available() > 0) {
            int oneByte = fileIn.read();
            if (!list.contains(oneByte))
                list.add(oneByte);
        }
        fileIn.close();
        Collections.sort(list, new Comparator<Integer>() {
                    public int compare(Integer bt1, Integer bt2) {
                        return bt1.compareTo(bt2);
                    }
                }
        );
        for (Integer i:list) {
            System.out.print(i + " ");
        }
    }
}
