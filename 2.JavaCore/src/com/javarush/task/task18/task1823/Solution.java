package com.javarush.task.task18.task1823;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/* 
Нити и байты
*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException, InterruptedException {
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            String inputStr = rd.readLine();
            if (inputStr.toLowerCase().equals("exit")) {
                rd.close();
                break;
            }
            ReadThread readThread = new ReadThread(inputStr);
            readThread.start();
            readThread.join();
        }
//        System.out.println(resultMap);
    }

    public static class ReadThread extends Thread {
        private String fileName;
        private volatile Map<Integer, Integer> ratingMap = new HashMap<>();
        private volatile Integer maxByte = 0;

        public ReadThread(String fileName) {
            //implement constructor body
            this.fileName = fileName;
            run();
        }
        // implement file reading here - реализуйте чтение из файла тут
        @Override
        public void run() {
            try {
                FileInputStream fis = new FileInputStream(fileName);
                int[] arr = new int[256];
                while (fis.available() > 0) {
                    int i = fis.read();
                    if (i >= 0 && i <= 256)
                        arr[i]++;
                }
                fis.close();
                synchronized (maxByte) {
                    int max = 0;
                    for (int i = 0; i < arr.length; i++) {
                        if (max < arr[i]) {
                            max = arr[i];
                            maxByte = i;
                        }
                    }
                    synchronized (resultMap) {
                        resultMap.put(fileName, maxByte);
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
