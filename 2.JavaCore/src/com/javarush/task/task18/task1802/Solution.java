package com.javarush.task.task18.task1802;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/* 
Минимальный байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fileIn = new FileInputStream(rd.readLine());
        int minByte = Integer.MAX_VALUE;
        while (fileIn.available() > 0) {
            int oneByte = fileIn.read();
            if (minByte >= oneByte) {
                minByte = oneByte;
            }
        }
        fileIn.close();
        System.out.println(minByte);
    }
}
