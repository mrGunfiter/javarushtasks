package com.javarush.task.task18.task1810;

/* 
DownloadException
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws DownloadException, IOException {
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        FileInputStream fileIn = null;

        while (true) {
            fileIn = new FileInputStream(rd.readLine());
            if (fileIn.available() < 1000) {
                fileIn.close();
                throw new DownloadException();
            }
        }
    }

    public static class DownloadException extends Exception {

    }
}
