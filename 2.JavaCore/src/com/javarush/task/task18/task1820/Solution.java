package com.javarush.task.task18.task1820;

/* 
Округление чисел
*/

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            String fileName1 = reader.readLine();
            String fileName2 = reader.readLine();

            FileInputStream fis = new FileInputStream(fileName1);
            FileOutputStream fos = new FileOutputStream(fileName2);
            List <Character> list = new ArrayList<Character>();
            while (fis.available() > 0) {
                int oneByte = fis.read();
                list.add((char)oneByte);
            }
            fis.close();
            String str = "";
            for (Character oneChar: list) {
                str += oneChar;
            }
            String[] words = str.split(" ");
            str = "";
            for (int i = 0; i < words.length; i++) {
                BigDecimal bigDigit = BigDecimal.valueOf(Float.parseFloat(words[i]));
                if (bigDigit.compareTo(new BigDecimal("0")) > 0) {
                    str += bigDigit.setScale(0, RoundingMode.HALF_UP);
                    str += " ";
                } else {
                    str += bigDigit.setScale(0, RoundingMode.HALF_DOWN);
                    str += " ";
                }
            }
//            System.out.println(str);
            fos.write(str.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
