package com.javarush.task.task18.task1807;

/* 
Подсчет запятых
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fileIn = new FileInputStream(rd.readLine());

        int count = 0;
        while (fileIn.available() > 0) {
            int oneByte = fileIn.read();
            if (oneByte == 44)
                count++;
        }
        fileIn.close();
        System.out.println(count);

    }
}
