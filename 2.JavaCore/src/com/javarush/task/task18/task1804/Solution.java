package com.javarush.task.task18.task1804;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/* 
Самые редкие байты
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fileIn = new FileInputStream(rd.readLine());
        Map<Integer, Integer> map = new HashMap<>();

        while (fileIn.available() > 0) {
            int oneByte = fileIn.read();
            map.put(oneByte, map.containsKey(oneByte)?(map.get(oneByte) + 1) : 1);
        }
        fileIn.close();
        int min = Integer.MAX_VALUE;
        for (Map.Entry<Integer, Integer> pair : map.entrySet()) {
            if (min > pair.getValue()) {
                min = pair.getValue();
            }
        }
        for (Map.Entry<Integer, Integer> pair : map.entrySet()) {
            if (min == pair.getValue()) {
                System.out.print(pair.getKey() + " ");
            }
        }
    }
}
