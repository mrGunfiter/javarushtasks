package com.javarush.task.task18.task1822;

/* 
Поиск данных внутри файла
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName;
        int id = Integer.parseInt(args[0]);
        fileName = reader.readLine();
        try {
            BufferedReader fileReader = new BufferedReader(new FileReader(fileName));
            while (fileReader.ready()) {
                String oneLine = fileReader.readLine();
                if (oneLine.startsWith(String.valueOf(id) + " ")) {
                    System.out.println(oneLine);
                    break;
                }
            }
            fileReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + fileName);
        }
    }
}
