package com.javarush.task.task18.task1809;

/* 
Реверс файла
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fileIn = new FileInputStream(rd.readLine());
        FileOutputStream fileOut = new FileOutputStream(rd.readLine());

            byte[] buffer = new byte[fileIn.available()];
            int count = fileIn.read(buffer);
            fileIn.close();
            int i = 0;
            int j = buffer.length - 1;
            byte tmp;
            while (j > i) {
                tmp = buffer[j];
                buffer[j] = buffer[i];
                buffer[i] = tmp;
                j--;
                i++;
            }
            fileOut.write(buffer, 0, count);
            fileOut.close();

    }
}
