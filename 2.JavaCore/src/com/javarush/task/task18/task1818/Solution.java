package com.javarush.task.task18.task1818;

/* 
Два в одном
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {

        try {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            String fileName0 = reader.readLine();
            String fileName1 = reader.readLine();
            String fileName2 = reader.readLine();

            FileOutputStream fos = new FileOutputStream(fileName0);
            FileInputStream fis1 = new FileInputStream(fileName1);
            FileInputStream fis2 = new FileInputStream(fileName2);
            int i = 0;
            while ((i = fis1.read()) != -1) {
                fos.write(i);
            }
            fis1.close();
            while ((i = fis2.read()) != -1) {
                fos.write(i);
            }
            fis2.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
