package com.javarush.task.task18.task1817;

/* 
Пробелы
*/

import java.io.FileInputStream;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {
        String fileName = args[0];

        FileInputStream fileInputStream = new FileInputStream(fileName);
        int i = 0;
        int countSpace = 0;
        int countSymbol = 0;
        while ((i = fileInputStream.read()) != -1) {
            if (i == 32)
                countSpace++;
            countSymbol++;
        }
        fileInputStream.close();
//        System.out.println("countSpace = " + countSpace);
//        System.out.println("countSymbol = " + countSymbol);
        System.out.println(String.format("%(.2f", (double)countSpace / (double)countSymbol * 100));
    }
}
