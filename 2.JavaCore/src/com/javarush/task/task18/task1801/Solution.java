package com.javarush.task.task18.task1801;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/* 
Максимальный байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fileIn = new FileInputStream(rd.readLine());
        int maxByte = 0;
        while (fileIn.available() > 0) {
            int oneByte = fileIn.read();
            if (maxByte < oneByte) {
                maxByte = oneByte;
            }
        }
        fileIn.close();
        System.out.println(maxByte);

    }
}
