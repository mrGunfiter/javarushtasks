package com.javarush.task.task18.task1821;

/* 
Встречаемость символов
*/

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;


public class Solution {

    public static void main(String[] args) throws FileNotFoundException {
        String fileName = args[0];
        Map <Integer, Integer> map = new HashMap<>();
        FileInputStream fis = new FileInputStream(fileName);
        int[] arr = new int[127];
        try {
            while (fis.available() > 0) {
                int i = fis.read();
//                if ((i >= 65 && i <= 90) || (i >= 97 && i <= 122))
                if (i >= 0 && i <= 127)
                    arr[i]++;
            }
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != 0)
                System.out.println((char)i + " " + arr[i]);
        }
    }
}
