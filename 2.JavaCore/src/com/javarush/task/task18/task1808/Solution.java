package com.javarush.task.task18.task1808;

/* 
Разделение файла
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
//        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
//        FileInputStream fileIn = new FileInputStream(rd.readLine());
//        FileOutputStream fileOut1 = new FileOutputStream(rd.readLine());
//        FileOutputStream fileOut2 = new FileOutputStream(rd.readLine());
//
//        byte[] buffer = new byte[fileIn.available() + 1 / 2];
//        int count = fileIn.read(buffer);
//        fileOut1.write( buffer, 0, count);
//        fileOut1.close();
//
//        buffer=new byte[fileIn.available() / 2];
//        count = fileIn.read(buffer);
//        fileIn.close();
//        fileOut2.write( buffer, 0, count);
//        fileOut2.close();

        BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));
        FileInputStream inputStream=new FileInputStream(reader.readLine());
        FileOutputStream outputStream=new FileOutputStream(reader.readLine());

        //обработка данных
        int a=inputStream.available();
        byte[] buffer=new byte[(a + 1) / 2];
        int count=inputStream.read(buffer);

        //записи по файлам
        outputStream.write(buffer,0,count);
        outputStream.close();
        outputStream=new FileOutputStream(reader.readLine());
        buffer=new byte[a / 2];
        count=inputStream.read(buffer);
        outputStream.write(buffer,0,count);

        //конечное освобождение памяти
        inputStream.close();
        outputStream.close();
    }
}
