package com.javarush.task.task06.task0622;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

/* 
Числа по возрастанию
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] myArray = new int[5];

        //напишите тут ваш код
        for (int i = 0; i < 5; i++) {
            myArray[i] = Integer.parseInt(reader.readLine());
        }
        Arrays.sort(myArray);
        for (int i = 0; i < myArray.length; i++) {
            System.out.print(myArray[i] + "\n");
        }
    }
}
