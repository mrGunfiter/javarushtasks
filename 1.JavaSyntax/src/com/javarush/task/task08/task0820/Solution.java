package com.javarush.task.task08.task0820;

import java.util.HashSet;
import java.util.Set;

/* 
Множество всех животных
*/

public class Solution {
    public static void main(String[] args) {
        Set<Cat> cats = createCats();
        Set<Dog> dogs = createDogs();

        Set<Object> pets = join(cats, dogs);
        printPets(pets);

        removeCats(pets, cats);
        printPets(pets);
    }

    public static Set<Cat> createCats() {
        Set<Cat> result = new HashSet<Cat>();

        //напишите тут ваш код
        result.add(new Cat("Марсик"));
        result.add(new Cat("Кузя"));
        result.add(new Cat("Борис"));
        result.add(new Cat("Маркиз"));
        return result;
    }

    public static Set<Dog> createDogs() {
        //напишите тут ваш код
        HashSet<Dog> result = new HashSet<Dog>();
        result.add(new Dog("пес1"));
        result.add(new Dog("пес2"));
        result.add(new Dog("пес3"));
        return result;
    }

    public static Set<Object> join(Set<Cat> cats, Set<Dog> dogs) {
        //напишите тут ваш код
        HashSet<Object> result = new HashSet<Object>();
        result.addAll(cats);
        result.addAll(dogs);
        return result;
    }

    public static void removeCats(Set<Object> pets, Set<Cat> cats) {
        //напишите тут ваш код
        for (Cat x : cats)
            pets.remove(x);
    }

    public static void printPets(Set<Object> pets) {
        //напишите тут ваш код
        for (Object pet : pets)
            System.out.println(pet);
    }

    //напишите тут ваш код
    public static class Cat{
        private String name;

        public Cat() {
            this.name = "qq";
        }

        public Cat(String name) {
            this.name = name;
        }
    }

    public static class Dog{
        private String name;

        public Dog() {
            this.name = "qq";
        }

        public Dog(String name) {
            this.name = name;
        }
    }
}
