package com.javarush.task.task08.task0816;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/* 
Добрая Зинаида и летние каникулы
*/

public class Solution {
    public Map<String, Date> createMap() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("MMMMM d yyyy", Locale.ENGLISH);
        Map<String, Date> map = new HashMap<>();
//        map.put("Сталлоне", dateFormat.parse("MAY 1 2012"));

        //напишите тут ваш код
        map.put("Stallone", dateFormat.parse("JUNE 1 1980"));
        map.put("Staleglone", dateFormat.parse("JANUARY 2 1981"));
        map.put("Staegllone", dateFormat.parse("FEBRUARY 21 1982"));
        map.put("Stalloegne", dateFormat.parse("NOVEMBER 4 1983"));
        map.put("Steallone", dateFormat.parse("AUGUST 16 1984"));
        map.put("Stane", dateFormat.parse("JULY 19 1985"));
        map.put("Stallqone", dateFormat.parse("SEPTEMBER 3 1950"));
        map.put("Stallonserge", dateFormat.parse("OCTOBER 6 1971"));
        map.put("Stalldxgone", dateFormat.parse("MAY 9 1988"));
        map.put("Stallonsgggge", dateFormat.parse("APRIL 1 1980"));

        return map;
    }

    public static void removeAllSummerPeople(Map<String, Date> map) {
        //напишите тут ваш код
        Iterator<Map.Entry<String, Date>> iterator = map.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry<String, Date> pair = iterator.next();
            if (pair.getValue().getMonth() > 4 && pair.getValue().getMonth() < 8)
                iterator.remove();
        }
    }

    public static void main(String[] args) {

    }
}
