package com.javarush.task.task08.task0813;

import java.util.HashSet;
import java.util.Set;

/* 
20 слов на букву «Л»
*/

public class Solution {
    public static Set<String> createSet() {
        //напишите тут ваш код
        HashSet<String> set = new HashSet<>();
        set.add("Лось");
        set.add("Лень");
        set.add("Лавка");
        set.add("Лобзик");
        set.add("Лопата");
        set.add("Леший");
        set.add("Лупа");
        set.add("Луна");
        set.add("Лето");
        set.add("Лапти");
        set.add("Леска");
        set.add("Ласка");
        set.add("Лото");
        set.add("Лотерея");
        set.add("Лазанья");
        set.add("Левый");
        set.add("Лишай");
        set.add("Лунатик");
        set.add("Локоть");
        set.add("Лошадь");

        return set;
    }

    public static void main(String[] args) {
        createSet();
    }
}
