package com.javarush.task.task08.task0818;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* 
Только для богачей
*/

public class Solution {
    public static Map<String, Integer> createMap() {
        //напишите тут ваш код
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        map.put("Иванов", 1000);
        map.put("Иванова", 2500);
        map.put("Коблучитенко", 2500);
        map.put("Ивановский", 499);
        map.put("Smith", 500);
        map.put("Васильев", 3000);
        map.put("Чапаев", 1256);
        map.put("Безруков", 800);
        map.put("Киллеров", 5000);
        map.put("Badsocks", 2600);
        return map;
    }

    public static void removeItemFromMap(Map<String, Integer> map) {
        //напишите тут ваш код
        Iterator<Map.Entry<String, Integer>> iterator = map.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry<String, Integer> pair = iterator.next();
            if (pair.getValue() < 500)
                iterator.remove();
        }
    }

    public static void main(String[] args) {

    }
}