package com.javarush.task.task08.task0821;

import java.util.HashMap;
import java.util.Map;

/* 
Однофамильцы и тёзки
*/

public class Solution {
    public static void main(String[] args) {
        Map<String, String> map = createPeopleList();
        printPeopleList(map);
    }

    public static Map<String, String> createPeopleList() {
        //напишите тут ваш код
        Map<String, String> map = new HashMap<String, String>();
        map.put("Иванов", "Ваня");
        map.put("Иванова", "Ванесса");
        map.put("Коблучитенко", "John");
        map.put("Иванов", "Иван");
        map.put("Smith", "John");
        map.put("Васильев", "Петр");
        map.put("Чапаев", "Василий");
        map.put("Безруков", "Сергей");
        map.put("Киллеров", "Остап");
        map.put("Badsocks", "John");
        return map;
    }

    public static void printPeopleList(Map<String, String> map) {
        for (Map.Entry<String, String> s : map.entrySet()) {
            System.out.println(s.getKey() + " " + s.getValue());
        }
    }
}
