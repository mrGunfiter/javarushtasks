package com.javarush.task.task08.task0815;

import java.util.HashMap;
import java.util.Map;

/* 
Перепись населения
*/

public class Solution {
    public static Map<String, String> createMap() {
        //напишите тут ваш код
        Map<String, String> map = new HashMap<String, String>();
        map.put("Иванов", "Ваня");
        map.put("Иванова", "Ванесса");
        map.put("Коблучитенко", "Вася");
        map.put("Ивановский", "Иван");
        map.put("Smith", "John");
        map.put("Васильев", "Петр");
        map.put("Чапаев", "Василий");
        map.put("Безруков", "Сергей");
        map.put("Киллеров", "Остап");
        map.put("Badsocks", "John");
        return map;
    }

    public static int getCountTheSameFirstName(Map<String, String> map, String name) {
        //напишите тут ваш код
        int count = 0;
        for (Map.Entry<String, String> pair : map.entrySet()){
            if (name.equals(pair.getValue())) ++count;
        }
        return count;
    }

    public static int getCountTheSameLastName(Map<String, String> map, String lastName) {
        //напишите тут ваш код
        int count = 0;
        for (Map.Entry<String, String> pair : map.entrySet()){
            if (lastName.equals(pair.getKey())) ++count;
        }
        return count;
    }

    public static void main(String[] args) {

    }
}
