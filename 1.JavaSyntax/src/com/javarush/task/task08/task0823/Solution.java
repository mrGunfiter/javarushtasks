package com.javarush.task.task08.task0823;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Омовение Рамы
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String string = reader.readLine();

        //напишите тут ваш код
        char[] c = string.toCharArray();
        if (c.length > 0) c[0] = Character.toUpperCase(c[0]);
        for (int i = 1; i < c.length; i++)
            if (Character.isSpaceChar(c[i - 1])) c[i] = Character.toUpperCase(c[i]);
        System.out.println(c);
    }
}
