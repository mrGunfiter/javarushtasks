package com.javarush.task.task08.task0812;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Cамая длинная последовательность
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        //напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        List<Integer> list = new ArrayList<Integer>(10);
        for (int i = 0; i < 10; i++) {
            list.add(Integer.parseInt(rd.readLine()));
        }

        int count = 1;
        int newCount = 1;
        for (int i = 0; i < list.size() - 1; i++) {
            if (list.get(i).intValue() == list.get(i + 1).intValue())
                newCount++;
            else {
                if (newCount > count)
                    count = newCount;
                newCount = 1;
            }
        }
        if (newCount > count)
            count = newCount;

        System.out.println(count);
    }
}