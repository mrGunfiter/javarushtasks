package com.javarush.task.task08.task0824;

import java.util.ArrayList;
import java.util.List;

/* 
Собираем семейство
*/

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        List<Human> ch0 = new ArrayList<Human>();
        Human son1 = new Human("Петя", true, 10, ch0);
        Human son2 = new Human("Вася", true, 12, ch0);
        Human dot1 = new Human("Оля", false, 4, ch0);
        List<Human> ch = new ArrayList<Human>();
        ch.add(son1);
        ch.add(son2);
        ch.add(dot1);
        Human father = new Human("Толя", true, 35, ch);
        Human mother = new Human("Галя", false, 32, ch);
        List<Human> children1 = new ArrayList<Human>();
        children1.add(father);
        List<Human> children2 = new ArrayList<Human>();
        children2.add(mother);

        Human grDad1 = new Human("Федор", true, 75, children1);
        Human grMot1 = new Human("Фекла", false, 70, children1);
        Human grDad2 = new Human("Васисаулий", true, 69, children2);
        Human grMot2 = new Human("Фаина", false, 63, children2);

        System.out.println(grDad1);
        System.out.println(grMot1);
        System.out.println(grDad2);
        System.out.println(grMot2);
        System.out.println(father);
        System.out.println(mother);
        System.out.println(son1);
        System.out.println(son2);
        System.out.println(dot1);
    }

    public static class Human {
        //напишите тут ваш код
        public String name;
        public boolean sex;
        public int age;
        public List<Human> children = new ArrayList<Human>();

        public Human(String name, boolean sex, int age, List<Human> children) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children = children;
        }

        public String toString() {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0) {
                text += ", дети: " + this.children.get(0).name;

                for (int i = 1; i < childCount; i++) {
                    Human child = this.children.get(i);
                    text += ", " + child.name;
                }
            }
            return text;
        }
    }
}
