package com.javarush.task.task09.task0922;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/* 
Какое сегодня число?
*/

public class Solution {

    private static DateFormatSymbols myDateFormatSymbols = new DateFormatSymbols()
    {
        @Override
        public String[] getShortMonths()
        {
            return new String[]{"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
        }
    };

    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        SimpleDateFormat dateFormatIn = new SimpleDateFormat("yyyy-MM-dd", myDateFormatSymbols);
        SimpleDateFormat dateFormatOut = new SimpleDateFormat("MMM dd, YYYY", myDateFormatSymbols);
        String str = rd.readLine();
        Date dt = dateFormatIn.parse(str);
        System.out.println(dateFormatOut.format(dt));
    }
}
