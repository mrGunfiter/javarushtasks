package com.javarush.task.task09.task0923;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Гласные и согласные
*/

public class Solution {
    public static char[] vowels = new char[]{'а', 'я', 'у', 'ю', 'и', 'ы', 'э', 'е', 'о', 'ё'};

    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        String str = rd.readLine();
        char[] chArray = str.toCharArray();
        String vowels = "";
        String noVowels = "";
        for (int i = 0; i < chArray.length; i++) {
            if (isVowel(chArray[i])){
                vowels += chArray[i];
                vowels += " ";
            }else if (chArray[i] != ' '){
                noVowels += chArray[i];
                noVowels += " ";
            }
        }
        System.out.println(vowels);
        System.out.println(noVowels);
    }

    // метод проверяет, гласная ли буква
    public static boolean isVowel(char c) {
        c = Character.toLowerCase(c);  // приводим символ в нижний регистр - от заглавных к строчным буквам
        for (char d : vowels) {  // ищем среди массива гласных
            if (c == d) {
                return true;
            }
        }
        return false;
    }
}