package com.javarush.task.task07.task0712;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Самые-самые
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list = new ArrayList<String>();

        for (int i = 0; i < 10; i++) {
            list.add(rd.readLine());
        }

        int minl = list.get(0).length();
        int maxl = list.get(0).length();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).length() < minl)
                minl = list.get(i).length();
            else if (list.get(i).length() > maxl)
                maxl = list.get(i).length();
        }

        for (int i = 0; i < list.size(); i++) {
            if ((list.get(i).length() == minl) || (list.get(i).length() == maxl)) {
                System.out.println(list.get(i));
                break;
            }
        }

    }
}
