package com.javarush.task.task07.task0704;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Переверни массив
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        int numb[] = new int[10];
        int tmp[] = new int[10];

        for (int i = 0; i < 10; i++) {
            numb[i] = Integer.parseInt(rd.readLine());
        }

        int j = numb.length - 1;
        for (int i = 0; i < numb.length; i++) {
            tmp[i] = numb[j];
            --j;
        }

        numb = tmp;

        for (int i = 0; i < numb.length; i++) {
            System.out.println(numb[i]);
        }
    }
}

