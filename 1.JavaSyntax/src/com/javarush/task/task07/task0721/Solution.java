package com.javarush.task.task07.task0721;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Минимаксы в массивах
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int maximum;
        int minimum;

        //напишите тут ваш код
        int[] numb = new int[20];
        for (int i = 0; i < 20; i++) {
            numb[i] = Integer.parseInt(reader.readLine());
        }
        maximum = numb[0];
        minimum = numb[0];
        for (int i = 0; i < numb.length; i++) {
            if (numb[i] >= maximum)
                maximum = numb[i];
            if (numb[i] <= minimum)
                minimum = numb[i];
        }


        System.out.print(maximum + " " + minimum);
    }
}
