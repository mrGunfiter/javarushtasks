package com.javarush.task.task07.task0724;

/* 
Семейная перепись
*/

public class Solution {
    public static void main(String[] args) {
        // напишите тут ваш код
        Human grFather1 = new Human("Вася", true, 65);
        Human grFather2 = new Human("Федя", true, 75);
        Human grMother1 = new Human("Фекла", false, 61);
        Human grMother2 = new Human("Гапуся", false, 72);
        Human father = new Human("Павел", true, 35, grFather1, grMother1);
        Human mother = new Human("Катя", false, 32, grFather2, grMother2);
        Human son1 = new Human("Игорь", true, 12, father, mother);
        Human son2 = new Human("Слава", true, 10, father, mother);
        Human dot = new Human("Аня", false, 6, father, mother);

        System.out.println(grFather1);
        System.out.println(grFather2);
        System.out.println(grMother1);
        System.out.println(grMother2);
        System.out.println(father);
        System.out.println(mother);
        System.out.println(son1);
        System.out.println(son2);
        System.out.println(dot);
    }

    public static class Human {
        // напишите тут ваш код
        public String name;
        public boolean sex;
        public int age;
        public Human father;
        public Human mother;

        public Human(String name, boolean sex, int age) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = null;
            this.mother = null;
        }

        public Human(String name, boolean sex, int age, Human father, Human mother) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = father;
            this.mother = mother;
        }

        public String toString() {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }
}