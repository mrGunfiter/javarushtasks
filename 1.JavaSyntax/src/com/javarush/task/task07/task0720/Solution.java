package com.javarush.task.task07.task0720;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Перестановочка подоспела
*/

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String Str1 = reader.readLine();
        String Str2 = reader.readLine();
        int N = Integer.parseInt(Str1);
        int M = Integer.parseInt(Str2);

        ArrayList<String> list = new ArrayList<String>();

        for (int i = 0; i < N; i++) {
            list.add(reader.readLine());
        }

        Str1 = null;
        int j = 0;
        for (int i = 0; i < M; i++) {
            Str1 = list.get(j);
            list.remove(j);
            list.add(Str1);
            j = 0;
        }

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }

}
