package com.javarush.task.task07.task0705;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Один большой массив и два маленьких
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        String str;
        int arrayBig[] = new int[20];
        int array1[] = new int[10];
        int array2[] = new int[10];

        for (int i = 0; i < 20; i++) {
            str = rd.readLine();
            arrayBig[i] = Integer.parseInt(str);
        }

        for (int i = 0; i < 10; i++) {
            array1[i] = arrayBig[i];
        }
        for (int i = 0; i < 10; i++) {
            array2[i] = arrayBig[i + 10];
        }

        for (int i = 0; i < 10; i++) {
            System.out.println(array2[i]);
        }
    }
}
