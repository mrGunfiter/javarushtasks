package com.javarush.task.task05.task0502;

/* 
Реализовать метод fight
*/

public class Cat {
    public int age;
    public int weight;
    public int strength;

    public Cat() {
    }

    public boolean fight(Cat anotherCat) {
        //напишите тут ваш код
        int win1 = 0;
        int win2 = 0;
        int win3 = 0;
        if (this.age > anotherCat.age)
            win1++;
        else if (this.age < anotherCat.age)
            win1--;

        if (this.strength > anotherCat.strength)
            win2++;
        else if (this.strength < anotherCat.strength)
            win2--;

        if (this.weight > anotherCat.weight)
            win3++;
        else if (this.weight < anotherCat.weight)
            win3--;

        if ((win1 + win2 + win3) > 0)
            return true;
        return false;
    }

    public static void main(String[] args) {

    }
}
