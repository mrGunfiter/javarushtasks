package com.javarush.task.task05.task0501;

/* 
Создание кота
*/

public class Cat {
    //напишите тут ваш код

        public String name;
        public int age;
        public int weight;
        public int strength;

        public void setName(String name) {
            this.name = name;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public void setWeight(int weight) {
            this.weight = weight;
        }

        public void setStrength(int strength) {
            this.strength = strength;
        }

        public int getStrength() {
            return strength;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }

        public int getWeight() {
            return weight;
        }

        public static void main(String[] args) {

        }

}
