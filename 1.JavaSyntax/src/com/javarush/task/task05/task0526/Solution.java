package com.javarush.task.task05.task0526;

/* 
Мужчина и женщина
*/

public class Solution {
    public static void main(String[] args)
    {
        Man man1 = new Man("Вася", 25, "деревня");
        Man man2 = new Man("Федя", 36, "город");
        Woman woman1 = new Woman("Фекла", 24, "сити");
        Woman woman2 = new Woman("Гапуся", 20, "село");

        System.out.println(man1.name + " " + man1.age + " " + man1.address);
        System.out.println(man2.name + " " + man2.age + " " + man2.address);
        System.out.println(woman1.name + " " + woman1.age + " " + woman1.address);
        System.out.println(woman2.name + " " + woman2.age + " " + woman2.address);
    }

    public static class Man {
        String name;
        int age;
        String address;

        public Man(String name, int age, String address) {
            this.name = name;
            this.age = age;
            this.address = address;
        }

        public Man(Man otherMan) {
            this.name = otherMan.name;
            this.age = otherMan.age;
            this.address = otherMan.address;
        }
    }
    public static class Woman {
        String name;
        int age;
        String address;

        public Woman(String name, int age, String address) {
            this.name = name;
            this.age = age;
            this.address = address;
        }

        public Woman(Woman otherWoman) {
            this.name = otherWoman.name;
            this.age = otherWoman.age;
            this.address = otherWoman.address;
        }


    }

}
