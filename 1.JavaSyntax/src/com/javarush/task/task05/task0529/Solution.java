package com.javarush.task.task05.task0529;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Консоль-копилка
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int summa = 0;
        String str = null;
        int n = 0;

        while (!"сумма".equals(str)) {
            str = rd.readLine();
            if (isInt(str)) {
                n = Integer.parseInt(str);
                summa += n;
            }
        }
        System.out.println(summa);
    }

    public static boolean isInt(String x) throws NumberFormatException
    {
        try {
            Integer.parseInt(x);
            return true;
        } catch(Exception e) {
            return false;
        }
    }

}
