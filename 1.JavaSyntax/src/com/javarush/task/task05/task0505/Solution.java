package com.javarush.task.task05.task0505;

/* 
Кошачья бойня
*/

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        Cat cat1 = new Cat("Кузя", 5, 5, 8);
        Cat cat2 = new Cat("Борис", 3, 3, 7);
        Cat cat3 = new Cat("Васька", 6, 4, 1);

        System.out.println(cat1.name + " vs " + cat2.name + " " + cat1.fight(cat2));
        System.out.println(cat1.name + " vs " + cat3.name + " " + cat1.fight(cat3));
        System.out.println(cat3.name + " vs " + cat2.name + " " + cat3.fight(cat2));
    }

    public static class Cat {
        protected String name;
        protected int age;
        protected int weight;
        protected int strength;

        public Cat(String name, int age, int weight, int strength) {
            this.name = name;
            this.age = age;
            this.weight = weight;
            this.strength = strength;
        }

        public boolean fight(Cat anotherCat) {
            int ageAdvantage = this.age > anotherCat.age ? 1 : 0;
            int weightAdvantage = this.weight > anotherCat.weight ? 1 : 0;
            int strengthAdvantage = this.strength > anotherCat.strength ? 1 : 0;

            int score = ageAdvantage + weightAdvantage + strengthAdvantage;
            return score > 2; // return score > 2 ? true : false;
        }
    }
}
