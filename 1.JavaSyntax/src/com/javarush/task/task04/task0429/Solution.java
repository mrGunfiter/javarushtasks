package com.javarush.task.task04.task0429;

/* 
Положительные и отрицательные числа
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        String A = rd.readLine();
        int a = Integer.parseInt(A);
        String B = rd.readLine();
        int b = Integer.parseInt(B);
        String C = rd.readLine();
        int c = Integer.parseInt(C);

        int pCount = 0;
        int mCount = 0;

        if (a > 0) ++pCount;
        if (b > 0) ++pCount;
        if (c > 0) ++pCount;
        if (a < 0) ++mCount;
        if (b < 0) ++mCount;
        if (c < 0) ++mCount;

        System.out.println("количество отрицательных чисел: " + mCount);
        System.out.println("количество положительных чисел: " + pCount);
    }
}
