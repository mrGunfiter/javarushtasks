package com.javarush.task.task04.task0424;

/* 
Три числа
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        String A = rd.readLine();
        int a = Integer.parseInt(A);
        String B = rd.readLine();
        int b = Integer.parseInt(B);
        String C = rd.readLine();
        int c = Integer.parseInt(C);

        if (a == b & a != c)
            System.out.println("3");
        if (a == c & a != b)
            System.out.println("2");
        if (b == c & b != a)
            System.out.println("1");
    }
}
