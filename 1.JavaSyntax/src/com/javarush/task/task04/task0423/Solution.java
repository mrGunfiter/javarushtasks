package com.javarush.task.task04.task0423;

/* 
Фейс-контроль
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        String Name = rd.readLine();
        String sAge = rd.readLine();
        int nAge = Integer.parseInt(sAge);

        if (nAge > 20)
            System.out.println("И 18-ти достаточно");

    }
}
