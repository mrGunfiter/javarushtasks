package com.javarush.task.task04.task0434;


/* 
Таблица умножения
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        int x = 1;
        int y = 1;
        int i = 0;
        int k = 0;
        while (k < 10)
        {
            i = 0;
            while (i < 10)
            {
                System.out.print(x * y + " ");
                y++;
                i++;
            }
            y = 1;
            x++;
            System.out.println(" ");
            k++;
        }
    }
}
