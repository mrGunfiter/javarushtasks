package com.javarush.task.task04.task0436;


/* 
Рисуем прямоугольник
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        String Str1 = rd.readLine();
        String Str2 = rd.readLine();
        int m = Integer.parseInt(Str1);
        int n = Integer.parseInt(Str2);
        int t = n;

        for (m = m; m > 0; m--) {
            for (n = n; n > 0; n--) {
                System.out.print("8");
            }
            System.out.println();
            n = t;
        }

    }
}
