package com.javarush.task.task04.task0441;


/* 
Как-то средненько
*/
import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        String Str1 = rd.readLine();
        String Str2 = rd.readLine();
        String Str3 = rd.readLine();
        int a = Integer.parseInt(Str1);
        int b = Integer.parseInt(Str2);
        int c = Integer.parseInt(Str3);

        if (a == b)
            System.out.println(a);
        else if (b == c)
            System.out.println(b);
        else if (a == c)
            System.out.println(c);
        else if (((a > b) && (a < c)) || ((a > c) && (a < b)))
            System.out.println(a);
        else if (((b > a) && (b < c)) || ((b > c) && (b < a)))
            System.out.println(b);
        else System.out.println(c);

    }
}
