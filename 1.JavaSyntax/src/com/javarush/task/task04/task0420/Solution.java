package com.javarush.task.task04.task0420;

/* 
Сортировка трех чисел
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());


        if ((a == b) && (a > c))
            System.out.println(a + " " + a + " " + c);
        else if ((a == b) && (a < c))
            System.out.println(c + " " + a + " " + a);
        else if ((a == c) && (a < b))
            System.out.println(b + " " + a + " " + a);
        else if ((a == c) && (a > b))
            System.out.println(a + " " + a + " " + b);
        else if ((b == c) && (a > b))
            System.out.println(a + " " + b + " " + b);
        else if ((b == c) && (a < b))
            System.out.println(b + " " + b + " " + a);

        else if ((a > b) && (a > c)){
            if (b > c)
                System.out.println(a + " " + b + " " + c);
            else
                System.out.println(a + " " + c + " " + b);
        }
        else {
            if ((b > a) && (b > c)) {
                if (a > c)
                    System.out.println(b + " " + a + " " + c);
                else
                    System.out.println(b + " " + c + " " + a);
            }
            else if ((c > a) && (c > b)) {
                if (a > b)
                    System.out.println(c + " " + a + " " + b);
                else
                    System.out.println(c + " " + b + " " + a);
            }
        }


    }
}
