package com.javarush.task.task04.task0428;

/* 
Положительное число
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        String A = rd.readLine();
        int a = Integer.parseInt(A);
        String B = rd.readLine();
        int b = Integer.parseInt(B);
        String C = rd.readLine();
        int c = Integer.parseInt(C);

        int Count = 0;

        if (a > 0) Count += 1;
        if (b > 0) Count += 1;
        if (c > 0) Count += 1;

        System.out.println(Count);

    }
}
