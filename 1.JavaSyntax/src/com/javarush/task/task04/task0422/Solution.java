package com.javarush.task.task04.task0422;

/* 
18+
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        String Name = rd.readLine();
        String sAge = rd.readLine();
        int nAge = Integer.parseInt(sAge);

        if (nAge < 18)
            System.out.println("Подрасти еще");

    }
}
