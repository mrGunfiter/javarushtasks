package com.javarush.task.task04.task0442;


/* 
Суммирование
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        String s = "";
        int n = 0;
        int Sum = 0;

        while (n != -1) {
            s = rd.readLine();
            n = Integer.parseInt(s);
            Sum += n;
        }
        System.out.println(Sum);

    }
}
