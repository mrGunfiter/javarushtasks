package com.javarush.task.task04.task0425;

/* 
Цель установлена!
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        String A = rd.readLine();
        int a = Integer.parseInt(A);
        String B = rd.readLine();
        int b = Integer.parseInt(B);

        if (a > 0 & b > 0)
            System.out.println("1");
        if (a < 0 & b > 0)
            System.out.println("2");
        if (a < 0 & b < 0)
            System.out.println("3");
        if (a > 0 & b < 0)
            System.out.println("4");

    }
}
