package com.javarush.task.task04.task0443;


/* 
Как назвали, так назвали
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        String Name = rd.readLine();
        String Str1 = rd.readLine();
        String Str2 = rd.readLine();
        String Str3 = rd.readLine();
        int y = Integer.parseInt(Str1);
        int m = Integer.parseInt(Str2);
        int d = Integer.parseInt(Str3);

        System.out.println("Меня зовут " + Name + ".");
        System.out.println("Я родился " + d + "." + m + "." + y);

    }
}
