package com.javarush.task.task10.task1013;

/* 
Конструкторы класса Human
*/

public class Solution {
    public static void main(String[] args) {
    }

    public static class Human {
        // Напишите тут ваши переменные и конструкторы
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public int getWeight() {
            return weight;
        }

        public void setWeight(int weight) {
            this.weight = weight;
        }

        public String getProffession() {
            return proffession;
        }

        public void setProffession(String proffession) {
            this.proffession = proffession;
        }

        public boolean isMarried() {
            return isMarried;
        }

        public void setMarried(boolean married) {
            isMarried = married;
        }

        private int age;
        private int height;
        private int weight;
        private String proffession;
        private boolean isMarried;

        public Human(String name, int age, int height) {
            this.name = name;
            this.age = age;
            this.height = height;
        }

        public Human(String name, int age, int height, int weight) {
            this.name = name;
            this.age = age;
            this.height = height;
            this.weight = weight;
        }

        public Human(String name, int age, int height, int weight, String proffession, boolean isMarried) {
            this.name = name;
            this.age = age;
            this.height = height;
            this.weight = weight;
            this.proffession = proffession;
            this.isMarried = isMarried;
        }

        public Human(String name, int age) {

            this.name = name;
            this.age = age;
        }

        public Human(String name) {

            this.name = name;
        }

        public Human(String name, boolean isMarried) {
            this.name = name;
            this.isMarried = isMarried;
        }

        public Human(String name, String proffession) {

            this.name = name;
            this.proffession = proffession;
        }

        public Human(int height, String proffession) {
            this.height = height;
            this.proffession = proffession;
        }

        public Human(int height, int weight, String proffession) {
            this.height = height;
            this.weight = weight;
            this.proffession = proffession;
        }

        public Human() {

        }
    }

}
