package com.javarush.task.task10.task1019;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/* 
Функциональности маловато!
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        HashMap<String, Integer> map = new HashMap<String, Integer>(10);
        String str = reader.readLine();
        while (str.length() != 0) {
            int id = Integer.parseInt(str);
            String name = reader.readLine();
            map.put(name, id);
            if (name.length() == 0) break;
            str = reader.readLine();
        }
        for (Map.Entry<String, Integer> pair : map.entrySet()) {
            int index = pair.getValue();
            String name = pair.getKey();
            System.out.println(index + " " + name);
        }

//        int id = Integer.parseInt(reader.readLine());
//        String name = reader.readLine();
//
//        System.out.println("Id=" + id + " Name=" + name);
    }
}
